const mysqlx = require("@mysql/xdevapi");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const compileBets = require("../constants/compile_bets");

module.exports = async (
  _,
  { query },
  {
    currentUser,
    teamDataLoader,
    competitionDataLoader,
    eSportEventDataLoader,
    sportEventDataLoader,
    betDataLoader,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    console.log(inputBet);
    if (!inputBet._id) {
      throw new Error("no bet _id was given!");
    }

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE); // get get user

    const betColl = await mysqlDb.getCollection("bets");
    let bets = await betColl.find(query).execute();

    bets = bets.fetchAll();

    bets = compileBets(
      mysqlDb,
      currentUser,
      null,
      bets,
      teamDataLoader,
      competitionDataLoader,
      eSportEventDataLoader,
      sportEventDataLoader
    );
    await session.close();
    return bets;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Getting bets",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
