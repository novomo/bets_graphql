const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");

const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");
const { pubsub } = require("../../../constants/pubsub");
const compileBet = require("../constants/compile_bet");

module.exports = async (
  _,
  { inputBet },
  {
    currentUser,
    teamDataLoader,
    competitionDataLoader,
    eSportEventDataLoader,
    sportEventDataLoader,
    betDataLoader,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    console.log(inputBet);
    if (!inputBet._id) {
      throw new Error("no bet _id was given!");
    }

    let session;
    try {
      session = await client.getConnection();
    } catch (err) {
      client = await reconnect();
      session = await client.getConnection();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE); // get get user

    const userTable = await mysqlDb.getTable("users");
    let user = await userTable
      .select()
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();

    user = user.fetchOne();
    let [users] = await session.query(
      `SELECT * FROM users where userID = ${currentUser}';`
    );
    user = users[0];

    if (user[9] == null) {
      throw new Error("Users default currency is not set!");
    }
    // add transaction

    const transactionID = await queryBuilder(
      session,
      "update",
      { ...inputBet.transaction, status: "complete" },
      null,
      "transactionID",
      inputBet.transaction.transactionID
    );

    let [balances] = await session.query(
      `SELECT * FROM balances where balanceID = ${inputBet.balanceID}';`
    );
    let balance = balances[0];

    const balanceID = await queryBuilder(
      session,
      "update",
      { amount: inputBet.transaction.amount + balance[4] },
      null,
      "balanceID",
      inputBet.balanceID
    );

    // format bet for collection

    let { transaction, ...sentBet } = inputBet;
    const betColl = await mysqlDb.getCollection("bets");
    let betID = await settingColl
      .modify("betID like :betID")
      .bind("betID", inputBet._id)
      .patch({ ...sentBet })
      .execute();
    
    const [bets] = await session.query(`UPDATE bets
    SET doc = JSON_SET(
      doc ,
      '$.${}' ,
      '${}'
    )
    WHERE
      id = '${inputBet._id}';`)
    console.log(betID);
    betID = betID.getGeneratedIds()[0];
    // compile bet and return

    let bet = await compileBet(
      mysqlDb,
      currentUser,
      betID,
      null,
      null,
      transaction.transactionID,
      teamDataLoader,
      competitionDataLoader,
      eSportEventDataLoader,
      sportEventDataLoader,
      betDataLoader
    );

    pubsub.publish("USER_CHANGED", {
      changedBet: { ...bet },
    });
    await session.close();
    return bet;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating bet",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
