const mysqlx = require("@mysql/xdevapi");
const queryBuilder = require("../../../data/mysql/query_builder");
const { toTimestamp } = require("../../../node_normalization/numbers");
const {
  currencyConverter,
} = require("../../../constants/currency_converter/converter");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

let { client, reconnect } = require("../../../data/mysql/sql_connection");
const { pubsub } = require("../../../constants/pubsub");
const compileBet = require("../constants/compile_bet");

module.exports = async (
  _,
  { inputBet },
  {
    currentUser,
    teamDataLoader,
    competitionDataLoader,
    eSportEventDataLoader,
    sportEventDataLoader,
    betDataLoader,
  }
) => {
  if (!currentUser) {
    throw new Error("Not Authorised");
  }
  try {
    console.log(inputBet);

    let session;
    try {
      session = await client.getSession();
    } catch (err) {
      client = await reconnect();
      session = await client.getSession();
    }
    const mysqlDb = await session.getSchema(MYSQL_DATABASE); // get get user

    const userTable = await mysqlDb.getTable("users");
    let user = await userTable
      .select()
      .where("userID = :userID")
      .bind("userID", currentUser)
      .execute();

    user = user.fetchOne();

    if (user[9] == null) {
      throw new Error("Users default currency is not set!");
    }

    const transactionID = await queryBuilder(
      session,
      "insert",
      { ...inputBet.transaction, userID: currentUser, status: "pending" },
      "transactions"
    );

    const balanceTable = await mysqlDb.getTable("balances");

    let balance = await balanceTable
      .select()
      .where("balanceID = :balanceID")
      .bind("balanceID", inputBet.balanceID)
      .execute();

    balance = balance.fetchOne();

    // format bet for collection
    const betColl = await mysqlDb.getCollection("bets");
    inputBet.defaultStake = await currencyConverter(
      balance[3],
      user[9],
      inputBet.stake
    );
    let betID = await betColl
      .add({
        ...inputBet,
      })
      .execute();
    betID = betID.getGeneratedIds()[0];
    // compile bet and return

    let bet = await compileBet(
      mysqlDb,
      currentUser,
      betID,
      null,
      null,
      transactionID,
      teamDataLoader,
      competitionDataLoader,
      eSportEventDataLoader,
      sportEventDataLoader,
      betDataLoader
    );

    pubsub.publish("USER_CHANGED", {
      changedBet: { ...bet },
    });
    await session.close();
    return bet;
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Adding bet",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
