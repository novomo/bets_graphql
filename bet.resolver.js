const addBet = require("./resolvers/add_bet");
const updateBet = require("./resolvers/update_bet");
const getBets = require("./resolvers/get_bets");
const { pubsub } = require("../../constants/pubsub");
const { withFilter } = require("graphql-subscriptions");

module.exports = {
  Subscription: {
    changedBet: {
      subscribe: withFilter(
        () => pubsub.asyncIterator("CHANGED_BET"),
        (payload, variables) => {
          // Only push an update if the comment is on
          // the correct repository for this operation
          //console.log(variables.user);
          //console.log(payload.changedBet.user._id);
          return (
            payload.changedBet.userID.toString() === variables.user.toString()
          );
        }
      ),
    },
  },
  Query: {
    getBets,
  },
  Mutation: {
    addBet,
    updateBet,
  },
};
