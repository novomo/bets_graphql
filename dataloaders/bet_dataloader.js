const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  MYSQL_DATABASE,
} = require("../../../env.js");

let { client, reconnect } = require("../../../data/mysql/sql_connection");

module.exports.getBetById = async (id) => {
  let session;
  try {
    session = await client.getConnection();
  } catch (err) {
    client = await reconnect();
    session = await client.getConnection();
  }

  let [bets] = await session.query(
    `SELECT JSON_TYPE(doc) FROM tips where JSON_EXTRACT('doc' , '$._id') = '${id}';;`
  );
  await session.close();
  return bets[0];
};

module.exports.getBetByIds = async (ids) => {
  return ids.map((id) => getHorseById(id));
};
