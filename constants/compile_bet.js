const compileSportsEvent = require("../../sportevents_graphql/constants/sportsevent_compiler");
const compileEsportsEvent = require("../../sportevents_graphql/constants/esportsevent_compiler");
module.exports = async (
  mysqlDb,
  userId,
  betId,
  bet,
  transaction,
  transactionId,
  teamDataLoader,
  competitionDataLoader,
  eSportEventDataLoader,
  sportEventDataLoader,
  betDataLoader
) => {
  let b, t;
  if (!bet) {
    b = await betDataLoader.load(betId);
  } else {
    b = bet;
  }

  console.log(b);

  if (!transaction) {
    const transactionTable = await mysqlDb.getTable("transactions");
    t = await transactionTable
      .select()
      .where("transactionID = :transactionID")
      .bind("transactionID", transactionId)
      .execute();

    t = t.fetchOne();
  } else {
    t = transaction;
  }

  return {
    ...b,
    transaction: { ...transaction },
    userID: userId,
    bets: b.bets.map((bet) => {
      let event = {},
        e;
      if (bet.eventType === "sports") {
        e = compileSportsEvent(
          bet.eventID,
          null,
          teamDataLoader,
          competitionDataLoader,
          sportEventDataLoader
        );
      } else if (bet.eventType === "eSports") {
        e = compileEsportsEvent(
          bet.eventID,
          null,
          teamDataLoader,
          competitionDataLoader,
          eSportEventDataLoader
        );
      }
      event[`${bet.eventType}`] = e;
      return { ...bet, event: event };
    }),
  };
};
