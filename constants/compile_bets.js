const sportsEventCompiler = require("../../sportevents_graphql/constants/sportsevent_compiler");
const esportsEventCompiler = require("../../sportevents_graphql/constants/esportsevent_compiler");

module.exports = async (
  mysqlDb,
  userId,
  betIds,
  bets,
  teamDataLoader,
  competitionDataLoader,
  eSportEventDataLoader,
  sportEventDataLoader,
  betDataLoader
) => {
  let b, t;
  if (!bets) {
    b = await betDataLoader.load(betIds);
  } else {
    b = bets;
  }

  console.log(b);
  const transactionTable = await mysqlDb.getTable("bets");
  return await Promise.all(
    b.map(async (bet) => {
      t = await transactionTable
        .select()
        .where("transactionID = :transactionID")
        .bind("transactionID", bet, transactionID)
        .execute();

      t = t.fetchOne();
      return {
        ...bet,
        transaction: { ...transaction },
        userID: userId,
        bets: b.bets.map((bet) => {
          let event = {},
            e;
          if (bet.eventType === "sports") {
            e = sportsEventCompiler(
              bet.eventID,
              null,
              teamDataLoader,
              competitionDataLoader,
              sportEventDataLoader
            );
          } else if (bet.eventType === "eSports") {
            e = esportsEventCompiler(
              bet.eventID,
              null,
              teamDataLoader,
              competitionDataLoader,
              eSportEventDataLoader
            );
          }
          event[`${bet.eventType}`] = e;
          return { ...bet, event: event };
        }),
      };
    })
  );
};
